public class Application{

	public static void main (String[] args){
		
		
		
		Bear favbear = new Bear();
		
		favbear.Color="Brown";
		favbear.Name="Harold";
		favbear.Strength = 1400;
		favbear.Growl();
		favbear.Bite();
		
		Bear strongbear = new Bear();
		
		strongbear.Color="White";
		strongbear.Name="Arnold";
		strongbear.Strength=1600;
		strongbear.Growl();
		strongbear.Bite();
		
		
		//A Sleuth of Bears
		
		Bear[] sleuth = new Bear[3];
		sleuth[0]=favbear;
		sleuth[1]=strongbear;
		
		sleuth[2] = new Bear();
		sleuth[2].Color ="Black and White";
		sleuth[2].Name="Berthold";
		sleuth[2].Strength = 1200;
		
		System.out.println(sleuth[2].Strength);
		//System.out.println(strongbear.Name);
		
	}	
}